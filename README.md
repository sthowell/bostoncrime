# Crime Prediction and Prevention in Boston

This project was completed as a part of the program "Applied Data Science: Machine Learning" from the [EPFL Extension School](https://www.extensionschool.ch/) .

The aim was to demonstrate skills thaught during the course, including data processing, exploatory analysis, machine learning and interpretation of the results.  

Crime prediction and prevention is a fascinating topic and I was curious to find out how machine learning with publicly available data could help to discover the origins of crime and possibly prevent it. 
Due to the vast amount of publicly available data at [https://data.boston.gov/](https://data.boston.gov/), I decided to analyze Boston's crime scene. 

The complete project is comiled in a Jupyter Notebook in the depository.

![img](img/poverty_vs_crime.png)
