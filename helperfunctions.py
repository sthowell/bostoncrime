# Some helper functions to for the capstone project

import re
import pandas as pd
import numpy as np
import geopandas as gp
import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon, MultiPolygon, LineString


def camelcase(string):
    # Source: https://www.includehelp.com/python/convert-a-string-to-camelcase.aspx
    string = re.sub(r'(/|_|-|\s)+', " ", string).title().replace(" ", "")
    return string[0].lower() + string[1:]


def discretize_crimes(df, lat, long, copy=False):
    """Discretize coordinates with lat and long """
    
    if copy == True:
        df_bin = df.copy()
    else:
        df_bin = df
        
    # Bin latitude and longitude
    slat = pd.cut(df[['OFFENSE_CODE_GROUP', 'Lat', 'Long']].Lat, lat)
    slong = pd.cut(df[['OFFENSE_CODE_GROUP', 'Lat', 'Long']].Long, long)

    # Ranges
    df_bin['LatBin'] = slat
    df_bin['LongBin'] = slong
    
    # Midpoints
    df_bin['LatBinMid'] = df_bin.LatBin.apply(lambda x: x.mid)
    df_bin['LongBinMid'] = df_bin.LongBin.apply(lambda x: x.mid)
    
    # Coordinates
    df_bin['Coords'] = list(zip(df_bin.LongBinMid, df_bin.LatBinMid,))
    
    return df_bin


def create_heatmap(df, lat, long, dl):
    """Create a heat map"""

    la_min = min(lat)
    lo_min = min(long)
    
    N = len(long)
    M = len(lat)
    
    heatmap = np.zeros((M,N), dtype=np.float)
    coords = df.Coords.values
    
    for (x, y) in coords:
        try:
            j = int((y-la_min)/dl)
            i = int((x-lo_min)/dl)
            heatmap[j, i] += 1
        except Exception: # Catches NaN coordinates
            pass
        
    return heatmap
    
    
def scatter_crime_map(df, features, gp_map):
    """Scatterplot with Lat, Long from df with map"""
    
    off_group = df[df.OFFENSE_CODE_GROUP.isin(features)].groupby(by='OFFENSE_CODE_GROUP')

    # Figure
    fig, axs = plt.subplots(3,3,figsize=(16, 12))
    axs = axs.flatten()

    # Counter
    i = 0

    for offense_code, group in off_group:
        
        # Map
        gp_map.plot(ax=axs[i], edgecolor='k', facecolor='none')
        
        # Plot
        axs[i].scatter(group.Long.values, 
                       group.Lat.values, 
                       s=1, alpha=0.1)
        
        # Axis
        axs[i].set_title(offense_code)
        axs[i].set_ylim([42.225, 42.4])
        axs[i].set_xlim([-70.95, -71.2])
        axs[i].invert_xaxis()
        axs[i].set_aspect('equal')
        i+=1

    plt.show()
    
    
    
def read_entertainment_data(filename):
    """Loads and processes entertainment data"""
    
    # Load cvs
    events = pd.read_csv(filename, usecols=[2, 3, 5, 6, 23, 24])
    events.dropna(inplace=True)
    events['geometry'] = gp.points_from_xy(events.gpsx, events.gpsy)
   
    
    # Convert data units
    events = gp.GeoDataFrame(events)
    events.crs = 'EPSG:2249'
    events.to_crs('EPSG:4326', inplace=True)
    events['Long'] = events.geometry.x
    events['Lat'] = events.geometry.y
    events = events[~events.duplicated()].drop(['gpsy', 'gpsx'], axis=1).copy()
    
    return events
    
    
def amenities_to_district(gdf, clf):
    d = []

    for i, amenity, geometry in gdf[['amenity', 'geometry']].itertuples():
        
        if isinstance(geometry, Point):
            d.append({
                'district': clf.predict([[geometry.x, geometry.y]])[0],
                'amenity': amenity
            })
        elif isinstance(geometry, Polygon):
            centroid = geometry.centroid
            d.append({
                'district': clf.predict([[centroid.x, centroid.y]])[0],
                'amenity': amenity
            })
        elif isinstance(geometry, MultiPolygon):
            centroid = geometry.centroid
            d.append({
                'district': clf.predict([[centroid.x, centroid.y]])[0],
                'amenity': amenity
            })        
        elif isinstance(geometry, LineString):
            centroid = geometry.centroid
            d.append({
                'district': clf.predict([[centroid.x, centroid.y]])[0],
                'amenity': amenity
            })        
        else:
            raise NotImplementedError('%s from amenity %s has not been implemented' % (geometry, amenity))
            
    return d
